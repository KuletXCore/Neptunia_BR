# ![Neptunia_BR](https://trello-attachments.s3.amazonaws.com/5b750c120cf3bb7f77ba1c6a/5b890058aaf9ee71e886b90d/be9d3f8911dd4877d408685a2055913c/ConquestLogo.png) 
A Battle Royale game based on the Neptunia Series made in UE4.

----

# Instructions

1. Get "[Git](http://git-scm.com/downloads)", "[Git-LFS](https://git-lfs.github.com)", and your GUI client of choice. ([Github Desktop](https://desktop.github.com), [GitKraken](https://www.gitkraken.com/), etc.)
2. Create your own branch! (Use [this guide](https://docs.microsoft.com/en-us/vsts/repos/git/git-branching-guidance?view=vsts))
3. Clone your copy of the project to your desired directory (using the Desktop app of choice)
4. Follow the instructions of this video: https://youtube.com/watch?v=v-DWH5JQiDo (Disable initialization of .gitattributes and .gitignore, for I already have a working one in this repo.)
5. Don't forget to submit to "Source Control" after saving and push your changes to your remote via your chosen desktop app!

(This project uses the 4.20 build.)

--------

If you have better or clearer instructions on how to set up Git with UE4, please do not hesitate to edit this file.
Thank you!

--------

# Useful Links

Trello - https://trello.com/b/H33OpNON/neptunia-br

--------

# WE HAVE THE  ["Battle Royale template"](https://sellfy.com/p/DG9f/), BABY!!!
![Thank you!](https://cdn.discordapp.com/attachments/458560978821316630/486124957684727818/unknown.png)

# Git-LFS is Back in action, but please consider subscribing so we can fund this thing!

Consider donating via [streamlabs](https://streamlabs.com/donate/kuletxcore) or [ko-fi](http://ko-fi.com/kuletxcore) so the project can live, and who knows?

You can also subscribe to the creator's [Patreon](http://patreon.com/kuletxcore) or [SubscribeStar](http://subscribestar.com/KuletXCore) if you want to support him monthly!
